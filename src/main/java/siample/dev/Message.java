package siample.dev;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Message {
	
	@Value("${spring.profiles.active}")
	private String selectedProfile;
	
	@Value("${msg}")
	private String message;

	public Message() {
		
	}

	public Message(String selectedProfile, String message) {
		this.selectedProfile = selectedProfile;
		this.message = message;
	}

	public String getSelectedProfile() {
		return selectedProfile;
	}

	public void setSelectedProfile(String selectedProfile) {
		this.selectedProfile = selectedProfile;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Message [selectedProfile=" + selectedProfile + ", message=" + message + "]";
	}

	
}
