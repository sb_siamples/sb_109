package siample.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Sb109Application {

	public static void main(String[] args) {
		ApplicationContext aContext = SpringApplication.run(Sb109Application.class, args);
		
		System.out.println(aContext.getBean("message"));
	}

}
